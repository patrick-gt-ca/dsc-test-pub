Configuration ConfigNetworkDNSandDomain
{
    param
    (
        [string[]]$NodeName = 'localhost',

        [String[]]$DnsServerAddress = @('1.0.0.1','1.1.1.1'),
        
        [ValidateSet("IPv4","IPv6")]
        [string]$AddressFamily = 'IPv4',

        [Parameter(Mandatory=$true)]
        [ValidateNotNullorEmpty()]
        [PSCredential] $Admincreds,

        [Parameter(Mandatory=$false)]
        [ValidateNotNullorEmpty()]
        [System.String] $Domain,
         
        [Parameter(Mandatory=$false)]
        [ValidateNotNullorEmpty()]
        [string] $JoinOU
    )

    Import-DscResource -Module PSDesiredStateConfiguration, xNetworking, xDSCDomainjoin

    $Interface=Get-NetAdapter|Where Name -Like "Ethernet*"|Select-Object -First 1
    $InterfaceAlias=$($Interface.Name)

    $Uname          = $Admincreds.UserName 
    $DomainUname    = $Domain + '\' + $Uname
    $Passwd         = $Admincreds.Password
    [System.Management.Automation.PSCredential ]$DomainCreds = New-Object System.Management.Automation.PSCredential ($DomainUname, $Passwd)

    $ChromeMSIFile          = 'Chrome.msi'
    $WVDAgent               = 'WVDAgent.msi'
    $WVDAgentBootLoader     = 'WVDAgentBootLoader.msi'
    $LogFileName            = 'DSCLogFile.txt'

    Set-Location -Path "..\.."

    $ChromePackagePath      = $PWD.Path + '\dsc\' + $ChromeMSIFile
    $AgentPackagePath       = $PWD.Path + '\dsc\' + $WVDAgent
    $BootLoaderPackagePath  = $PWD.Path + '\dsc\' + $WVDAgentBootLoader
    $LogFilePath            = $PWD.Path + '\dsc\' + $LogFileName

    #$token = ''
    
    $ChromeArguments        = "/q /l $LogFilePath /norestart /passive"
    # '"/i C:\Support\RDInfraAgentInstall\RDAgent.msi", "/quiet", "/qn", "/norestart", "/passive", "REGISTRATIONTOKEN=$token", "/l* C:\Support\RDAgentInstall.txt"'
    #$DVIAgentArguments      = "/i $AgentPackagePath /q /l REGISTRATIONTOKEN=$token" 
    # '"/i C:\Support\RDAgentBootLoaderInstall\RDBootLoader.msi", "/quiet", "/qn", "/norestart", "/passive", "/l* C:\Support\RDBootLoaderInstall.txt"'
    $DVIBootLoaderArguments = '/i $BootLoaderPackagePath /q /l'

     Node localhost
     {
        #--------------------- [ allow reboot ] ---------------------------------------

        LocalConfigurationManager
        {
            RebootNodeIfNeeded = $true
        }

        #--------------------- [ change dns ] -----------------------------------------

        xDnsServerAddress DnsServerAddress
        {
            Address        = $DnsServerAddress
            InterfaceAlias = $InterfaceAlias
            AddressFamily  = $AddressFamily
        }

        <#--------------------- [ install msi ] -----------------------------------

        Name  : Remote Desktop Agent Boot Loader
        Value : {A38EE409-424D-4A0D-B5B6-5D66F20F62A5}

        Name  : Google Chrome
        Value : {B4C63984-9C49-3934-AE54-26B97EB7D531}

        Name  : Remote Desktop Services Infrastructure Agent
        Value : {6CC7B112-BC64-4257-B0D5-0664C8EA2985}

        Name  : Mozilla Firefox 73.0.1 x64 en-US
        Value : {1294A4C5-9977-480F-9497-C0EA1E630130}
        #>

        Package ChromeInstall
        {
            Ensure      = "Present"
            Path        = $ChromePackagePath
            Name        = "Google Chrome"
            ProductId   = "B4C63984-9C49-3934-AE54-26B97EB7D531"
            Arguments   = $ChromeArguments
        }

        <# force reboot sample code
        xPendingReboot RebootAfterPromotion {
            Name = "RebootAfterDCPromotion"
            DependsOn = "[xADDomainController]BDC"
        }
        #>

    }
}