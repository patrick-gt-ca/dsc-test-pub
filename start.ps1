Set-ExecutionPolicy -ExecutionPolicy "Unrestricted" -Force

#$url = 'https://azdump20200312.blob.core.windows.net/windows-powershell-dsc/ConfigNetworkDNSandDomain.ps1.zip'

# create temp with zip extension (or Expand will complain)
#$tmp = New-TemporaryFile | Rename-Item -NewName { $_ -replace 'tmp$', 'zip' } 

#download
#Invoke-WebRequest -OutFile $tmp $url

#exract to same folder 
$zipFile = "$PWD\ConfigNetworkDNSandDomain.ps1.zip"

# wait for download
$i = 180
do {
    Write-Host "." -NoNewline                
    Start-Sleep -Seconds 5
} until (Test-Path $zipFile -PathType Leaf)


#exract to same folder 
# $tmp | Expand-Archive -DestinationPath $PSScriptRoot -Force
$zipFile | Expand-Archive -DestinationPath $PSScriptRoot -Force

# remove temporary file
#$tmp | Remove-Item

. "ConfigNetworkDNSandDomain.ps1"