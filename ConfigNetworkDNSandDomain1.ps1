Configuration ConfigNetworkDNSandDomain
{
    param
    (
        [string[]]$NodeName = 'localhost',

        [String[]]$DnsServerAddress = @('8.8.8.8','8.8.4.4'),
        
        [ValidateSet("IPv4","IPv6")]
        [string]$AddressFamily = 'IPv4',
        
        [Parameter(Mandatory=$false)]
        [ValidateNotNullorEmpty()]
        [PSCredential] $Admincreds,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullorEmpty()]
        [string] $msiFilesPath,

        [Parameter(Mandatory=$false)]
        [ValidateNotNullorEmpty()]
        [System.String] $Domain = 'mydomain.local',
         
        [Parameter(Mandatory=$false)]
        [ValidateNotNullorEmpty()]
        [string] $JoinOU = 'OU=TestOU,DC=mydomain,DC=local'
        
    )

    #Install-Module -Name PSDesiredStateConfiguration, xNetworking, xDSCDomainjoin, xPowerShellExecutionPolicy
    Import-DscResource -Module PSDesiredStateConfiguration, xNetworking, xDSCDomainjoin, xPowerShellExecutionPolicy

    $Interface=Get-NetAdapter|Where Name -Like "Ethernet*"|Select-Object -First 1
    $InterfaceAlias=$($Interface.Name)

    $Uname          = $Admincreds.UserName 
    $DomainUname    = $Domain + '\' + $Uname
    $Passwd         = $Admincreds.Password
    [System.Management.Automation.PSCredential ]$DomainCreds = New-Object System.Management.Automation.PSCredential ($DomainUname, $Passwd)

    $ChromeMSIFile          = 'Chrome.msi'
    $WVDAgent               = 'WVDAgent.msi'
    $WVDAgentBootLoader     = 'WVDAgentBootLoader.msi'
    $LogFileName            = 'csutomScriptLog.txt'

    $ChromePackagePath      = $msiFilesPath + '\' + $ChromeMSIFile
    $AgentPackagePath       = $msiFilesPath + '\' + $WVDAgent
    $BootLoaderPackagePath  = $msiFilesPath + '\' + $WVDAgentBootLoader
    $LogFilePath            = $msiFilesPath + '\' + $LogFileName

    #$token = ''
    
    $ChromeArguments        = "/q /l $LogFilePath /norestart /passive"
    # '"/i C:\Support\RDInfraAgentInstall\RDAgent.msi", "/quiet", "/qn", "/norestart", "/passive", "REGISTRATIONTOKEN=$token", "/l* C:\Support\RDAgentInstall.txt"'
    #$DVIAgentArguments      = "/i $AgentPackagePath /q /l REGISTRATIONTOKEN=$token" 
    # '"/i C:\Support\RDAgentBootLoaderInstall\RDBootLoader.msi", "/quiet", "/qn", "/norestart", "/passive", "/l* C:\Support\RDBootLoaderInstall.txt"'
    $DVIBootLoaderArguments = '/i $BootLoaderPackagePath /q /l'

     Node localhost
     {
        #--------------------- [ allow reboot ] ---------------------------------------

        LocalConfigurationManager
        {
            RebootNodeIfNeeded = $true
        }

        xPowerShellExecutionPolicy ExecutionPolicy
        {
            ExecutionPolicy      = 'RemoteSigned'
            ExecutionPolicyScope = 'LocalMachine'
        }

        Service StartWimRM
        {
            Name        = "WinRM"
            StartupType = "Manual"
            State       = "Running"
            DependsOn = '[xPowerShellExecutionPolicy]ExecutionPolicy'
        }

        #--------------------- [ change dns ] -----------------------------------------
        
        xDnsServerAddress DnsServerAddress
        {
            Address        = $DnsServerAddress
            InterfaceAlias = $InterfaceAlias
            AddressFamily  = $AddressFamily
            DependsOn = '[Service]StartWimRM'
        }

        
        <#--------------------- [ install msi ] -----------------------------------

        Name  : Remote Desktop Agent Boot Loader
        Value : {A38EE409-424D-4A0D-B5B6-5D66F20F62A5}

        Name  : Google Chrome
        Value : {B4C63984-9C49-3934-AE54-26B97EB7D531}

        Name  : Remote Desktop Services Infrastructure Agent
        Value : {6CC7B112-BC64-4257-B0D5-0664C8EA2985}

        Name  : Mozilla Firefox 73.0.1 x64 en-US
        Value : {1294A4C5-9977-480F-9497-C0EA1E630130}
        #

        Package ChromeInstall
        {
            Ensure      = "Present"
            Path        = $ChromePackagePath
            Name        = "Google Chrome"
            ProductId   = "B4C63984-9C49-3934-AE54-26B97EB7D531"
            Arguments   = $ChromeArguments
            DependsOn = '[Service]StartWimRM'
        } 

        <# force reboot sample code
        xPendingReboot RebootAfterPromotion {
            Name = "RebootAfterDCPromotion"
            DependsOn = "[xADDomainController]BDC"
        }
        
        <# command line arguments for Agent install
        $MSIAgentArguments                      = @(
                                                        "/i"
                                                        "/qn"
                                                        "/norestart"
                                                        "/passive"
                                                        "/l* C:\Temp\RDAgentInstall.txt"
                                                        "REGISTRATIONTOKEN=$token"
                                                    )

        # command line arguments for BootLoader install 
        $MSIBootLoaderArguments                 = @(
                                                        "/i"
                                                        "/qn"
                                                        "/norestart"
                                                        "/passive"
                                                        "/l* C:\Temp\RDBootLoaderInstall.txt"
                                                    )                                                

        # Start-Process -FilePath "msiexec.exe" -ArgumentList "/i C:\Support\RDInfraAgentInstall\RDAgent.msi", "/quiet", "/qn", "/norestart", "/passive", "REGISTRATIONTOKEN=$token", "/l* C:\Support\RDAgentInstall.txt" -Wait -Passthru
        # Start-Process -FilePath "msiexec.exe" -ArgumentList "/i C:\Support\RDAgentBootLoaderInstall\RDBootLoader.msi", "/quiet", "/qn", "/norestart", "/passive", "/l* C:\Support\RDBootLoaderInstall.txt" -Wait -Passthru
    
        $AgentPackagePath ="$PWD\dsc\WVDAgent.msi"
        $BootLoaderPackagePath ="$PWD\dsc\WVDAgentBootLoader.msi"


        # Install the Windows Virtual Desktop Agent.
        xPackage AgentInstall {
            Ensure = "Present"
            Path = $AgentPackagePath
            Name = "Windows Virtual Desktop Agent"
            ProductId = "6CC7B112-BC64-4257-B0D5-0664C8EA2985"
            Arguments = $MSIAgentArguments
        }

        # Install the Windows Virtual Desktop Agent Bootloader
        xPackage BootLoaderInstall {
            Ensure = "Present"
            Path = $BootLoaderPackagePath
            Name = "Windows Virtual Desktop Agent Bootloader"
            ProductId = "A38EE409-424D-4A0D-B5B6-5D66F20F62A5"
            Arguments = $MSIBootLoaderArguments
        }
        #>

     }
 }